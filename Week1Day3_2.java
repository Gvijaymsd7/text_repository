package Week1;

public class Week1Day3_2 
{
 

		  public static void main(String[]args) 
		  {

		  Vehicle vehicle = new Vehicle();  // Create a Vehicle object

		  Vehicle scooter = new Scooter();  // Create a Scooter object

		  Vehicle car = new Car();  // Create a Car object

		              vehicle.noOfWheels();     //call noOfWheels of vehicle class

		              scooter.noOfWheels();   //call noOfWheels of Scooter class

		              car.noOfWheels();         // call noOfWheels of Car class

		  }

		}

